package forprofile;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class exhibits some simple, if time-consuming behaviour.
 * 
 * SlowString automatically generates a random string between
 * {@link #MIN_LENGTH} and {@link #MAX_LENGTH} characters long, each with
 * randomly generated characters. It is capable of detecting whether it has
 * generated a given string before.
 * 
 * The main method effectively acts as a basic test harness. It generates
 * {@link #NUM_SAMPLES} SlowString objects, and then at the end of the
 * generation process, reports how many (if any) duplicates occurred and what
 * they were.
 * 
 * The primary performance problem is that this takes IRO 1 minute for 100000
 * samples.
 * 
 * @author david
 *
 */
public class SlowString {
	public String getGenString() {
		return genString;
	}

	/**
	 * The first character, in ascending integral order, that should be
	 * considered for inclusion in the String. i.e. the String will only contain
	 * characters bounded between {@link #MIN_POSS_CHAR} and
	 * {@link #MAX_POSS_CHAR}
	 */
	private static final char MIN_POSS_CHAR = 'A';
	/**
	 * The final character, in ascending integral order, that should be
	 * considered for inclusion in the String. i.e. the String will only contain
	 * characters bounded between {@link #MIN_POSS_CHAR} and
	 * {@link #MAX_POSS_CHAR}
	 */
	private static final char MAX_POSS_CHAR = 'z';
	/**
	 * The minimum length of resulting Strings
	 */
	private static final int MIN_LENGTH = 5;
	/**
	 * The maximum length of resulting Strings
	 */
	private static final int MAX_LENGTH = 10;
	/**
	 * The number of samples generated
	 */
	private static final int NUM_SAMPLES = 100000;
//	/**
//	 * The duration of a second, in milliseconds
//	 */
//	private static final int SECS = 1000;

	/**
	 * A random number generator used across all instances
	 */
	private static Random random = new Random();

	/**
	 * A static record of which strings have been issued already
	 */
	private static List<String> checkers = new ArrayList<>();
	/**
	 * A randomly-generated String for this instance.
	 */
	private String genString;

	/**
	 * Constructs a new SlowString, in accordance with the settings represented
	 * by the class CONSTANTS.
	 */
	public SlowString() {
		int len = random.nextInt(MAX_LENGTH - MIN_LENGTH) + MIN_LENGTH;
		char[] forString = new char[len];
		for (int ch = 0; ch < len; ch++) {
			forString[ch] = (char) (random.nextInt((int) (MAX_POSS_CHAR - MIN_POSS_CHAR)) + MIN_POSS_CHAR);
		}
		this.genString = new String(forString);
	}

	/**
	 * Check if this SlowString has already been generated.
	 * 
	 * @return true if so, false otherwise
	 */
	public boolean checkExists() {
		String another = genString.intern();
		boolean exists = checkers.contains(another);

		if (exists) {
		} else
			checkers.add(another);
		return exists;
	}

	/**
	 * This main method acts as a test harness for SlowString. It demonstrates
	 * the performance issue outlined in the main class documentation
	 * 
	 * @param args
	 *            ignored
	 * @throws InterruptedException
	 *             in accordance with Thread.sleep()
	 */
	public static void main(String[] args) throws InterruptedException {
//		Commented out as we can make Debug Mode pause here
//		Thread.sleep(10 * SECS);// delay so we can attach profiler. Try 15*SECS,
//								// or even 20*SECS if required.

		List<SlowString> generatedSlowStrings = new ArrayList<>();
		List<String> identicals = new ArrayList<>();

		for (int i = 0; i < NUM_SAMPLES; i++) {
			SlowString genSlowString = new SlowString();
			generatedSlowStrings.add(genSlowString);
			if (genSlowString.checkExists()) {
				identicals.add(genSlowString.getGenString());
				System.out.println("What are the chances?");
			}
		}

		generatedSlowStrings.clear();

		System.out.println("generated " + identicals.size() + " duplicate strings");
		for (String identical : identicals)
			System.out.println(identical);
		System.out.println("--");
	}

}
